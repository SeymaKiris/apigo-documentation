==========
mutual TLS
==========

.. figure:: ../docs/images/mTLS/Figure1.ApiGo_gateway_manages_token_validation.png
    :alt: apigo gateway manages token validation

Identification of the third parties under PSD2 and Open Banking requires the use of electronic identification, authentication, and trust services (eIDAS), an EU-defined approach for companies to irrefutably present their identity. The certificates used are provided by a TPP to the ASPSP using the Transport Layer Security (TLS) protocol.

.. figure:: ../docs/images/mTLS/Figure2.TPP_Identification.png
    :alt: apigo TPP identification

There are several important challenges related to TPP identification. `ApiGo`_ provides the mutual TLS (mTLS) termination and validation, to help ASPSP and TPP’s against the challenges of implementing, operating, and maintaining complexity.

.. _ApiGo: https://apigo.com/

--------------
eIDAS and PSD2
--------------

When a regulated TPP entity wishes to use eIDAS Qualified Certificates for the purposes of consuming PSD2 APIs, there are at least two other parties that come into play. 

The first will be the National Competent Authority (NCA) serving the jurisdiction of that TPP. The NCA is responsible for vetting and validating the TPP organisation and ultimately issuing a TPP Authorisation Number, which will be maintained in a public registry. In other words, the NCA in the TPP’s jurisdiction acts as the source of truth regarding whether a particular TPP is regulated to perform transactions under PSD2 and which PSD2 services (such as Account Information or Payment Initiation) should be offered to said TPP.

Once the TPP has obtained an Authorisation Number from the NCA, they can then obtain Qualified Certificates from a Qualified Trust Service Provider (QTSP) that will include this number as a means of cryptographically asserting the TPP’s identity to other parties (such as ASPSPs).

Two different types of certificates come into play here, and TPPs can obtain and use one or both of the following:
    * Qualified Website Authentication Certificate (QWAC)
    * Qualified Certificate for Seals (QSealC)

It would be convenient to assume that, since a QWAC is just a rather specialised X509 certificate, any ASPSP could simply adopt Mutual TLS as the client authentication mechanism for TPP OAuth clients and thus get eIDAS support “for free,” provided that their OAuth Authorisation Server supports this capability.

-------------------
What is Mutual TLS?
-------------------

Mutual TLS (mTLS) authentication ensures that traffic is both secure and trusted in both directions between a TPP and the ASPSP. TPP can request that do not log in with an identity provider to demonstrate that they can reach a given resource. This TLS-connection is set up by the TPP. 

The TLS-connection has to be established always including client (i.e. TPP) authentication. Mutual TLS support in IdentityServer allows for two features:
    * Client authentication to endpoints within IdentityServer using a X.509 client certificate
    * Use of sender-constrained access tokens from a client to APIs using a X.509 client certificate

------------------------------
How ApiGo Supports mutual TLS?
------------------------------

`ApiGo`_ has support for mutual TLS in authentication - creating keys based on certificates. For this authentication the TPP has to use a qualified certificate for website authentication. This qualified certificate has to be issued by a qualified trust service provider according to the eIDAS regulation. The content of the certificate has to be compliant with the requirements of [EBA-RTS]. The certificate of the TPP has to indicate all roles the TPP is authorised to use.

------------
Certificates
------------

To request a client certificate when the client request is over TLS/SSL and validate the certificate is called TLS mutual authentication or client certificate authentication. 

Client certificate authentication is also a second layer of security for team members who both log in with an identity provider (IdP) and present a valid client certificate. With a root certificate authority (CA) in place, Access only allows requests from devices with a corresponding client certificate. When a request reaches the application, 

Access responds with a request for the client to present a certificate. If the device fails to present the certificate, the request is not allowed to proceed. If the client does have a certificate, Access completes a key exchange to verify.

---------------------
Client Authentication 
---------------------

Clients can use a X.509 client certificate as an authentication mechanism to endpoints in IdentityServer. At the TLS level, ASPSP only clients who provide client certificates that are verified and trusted by the server. ApiGo allows you to define a qualified certificate has to be issued by a qualified trust service provider according to the eIDAS regulation at the Gateway (global) level.

ApiGo can be configured to guess a user authentication key based on the provided client certificate. In other words, a user does not need to provide any key, except the certificate, and ApiGo will be able to identify the user, apply policies, and do the monitoring - the same as with regular Keys.

The basic idea here is that you can create a key based on a provided certificate, and this key will be used for the users with same client certificates.

.. figure:: ../docs/images/mTLS/Figure3.Client_authenticated_TLS_handshake.png
    :alt: apigo client authenticated TLS handshake

.. meta::
  :description: Click now to get information about mutual TLS, which ensures that traffic is safe and reliable in both directions between TPP and ASPSP!