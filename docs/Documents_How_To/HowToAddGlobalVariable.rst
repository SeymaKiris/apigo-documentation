================================
`How To Add a Global Variable?`_
================================

.. _How To Add a Global Variable?: https://apigo.com/admin/global-variables

What is Global Variable?
------------------------

Global variables, as the name suggests, are variables that can be accessed globally or throughout the application. The global variable is a variable with global scope, it can be defined and accessed in the Management Portal. Once defined, they remain in memory for the duration of the application's runtime. 

A Global variable is useful when multiple functions are accessing the same data. It is ideally used for storing "constants" as it helps you keep the consistency. The global variable belongs to every function in the program. A global variable can potentially be modified from the admin panel and any part of the application may depend on it.

How It Works?
-------------

1.	Tenant admin can declare global variables on Management Portal. The admin can access the global variables from all the functions or menus in ApiGo. Global Variables can be managed on the Admin Panel to prevent very long declarations and large allocations of duplicated data.  

`Management Portal`_ -> Configurations -> Global Variables -> Add New

.. _Management Portal: https://apigo.com/admin

.. figure:: ../../docs/images/GlobalVariables/GlobalVariablePicture1.png
    :alt: apigo global variables

2) Tenant admin can define a global variable that has a key to call and essential value. The key will be defined in double curly brackets. The preview can be seen on the head of the content box. There is Save and Delete options to execute for every function in the ApiGo. For more global variables, tenant admin also can click on Add New.

.. figure:: ../../docs/images/GlobalVariables/GlobalVariablePicture2.png
    :alt: apigo add global variable

3) This means that at any point global variables can affect the program as a whole. For example, tenant admin can use the defined global variable for documentation. When the global variable’s key is added as a documented content, the value corresponding to the key can be monitored by developers.

.. figure:: ../../docs/images/GlobalVariables/GlobalVariablePicture3.png
    :alt: apigo variable document view

4) Tenant admin can use global variables when multiple functions need to access the data. The reflection of the changes for documentation can be viewed on the Developer Portal’s Documents page. A reference to multiple functions such as a document headline that needs to be accessed across the application can be given.

.. figure:: ../../docs/images/GlobalVariables/GlobalVariablePicture4.png
    :alt: apigo global variable view

.. meta::
  :description: Click now to learn how to add a global variable to the ApiGo Open Banking, to get detailed information and to review our document!