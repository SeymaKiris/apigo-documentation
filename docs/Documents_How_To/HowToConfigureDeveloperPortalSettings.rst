=====================================
`How To Configure Developer Portal?`_
=====================================

.. _How To Configure Developer Portal?: https://apigo.com/admin/site-settings

What is Developer Portal Settings?
----------------------------------

Developer portal is an automatically generated, fully customizable website with the documentation of your APIs. It is where developers can discover your APIs, learn how to use them, and request access. Following section will explain how to navigate its administrative interface, customize the contents, publish the changes, view the published portal.

How It Works?
-------------

1) The Developer Portal branding, general layout, page structure, appearance and behaviors are controlled by Developer Portal Settings. Management Portal provide a solution to manage the configurations as an administrator. Follow the steps below to access the managed version of the portal. If you are accessing the portal for the first time, the default content will be automatically provisioned in the background. Default content has been designed to showcase portal's capabilities and minimize the number of customizations needed to personalize your portal. 

`Management Portal`_ -> Configurations -> Developer Portal Settings

.. _Management Portal: https://apigo.com/admin

.. figure:: ../../docs/images/ConfigureDeveloperPortalSettings/ConfigureDeveloperPortalSettingsPicture1.png
    :alt: apigo developer portal settings

2) The menu items on the center let you modify the layout elements of Developer Portal's main page and specify how to modify those elements. There are six main functions to customize Developer Portal Settings.

2.1)	How to Customize Developer Portal Header?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The configuration setting has a two main appearance component to customize Developer Portal. Any color can be selected to encolour Developer Portal’s header. Site Header can be used to expand the set of choices and customize your theme. The portal diversifications can be carried out and reflect just in time. The real-time changes will be seen in the portal.

.. figure:: ../../docs/images/ConfigureDeveloperPortalSettings/ConfigureDeveloperPortalSettingsPicture2.png
    :alt: apigo configure developer portal settings

2.2)	How to Customize Developer Portal Logo?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ApiGo users can customize Developer Portal logo which will be taken a place in the upper left corner. “Click to upload” button provides to get any image to display on the Developer Portal. The bank’s logo can be selected from the machine used. After the configurations have done, click on “Save” button, and check the updates from Developer Portal. 

.. figure:: ../../docs/images/ConfigureDeveloperPortalSettings/ConfigureDeveloperPortalSettingsPicture3.png
    :alt: apigo developer portal logo

2.3)	How to Customize Home Swiper?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Swiper is a modern touch slider that is focused on modern apps/platforms to bring the best experience and simplicity. ApiGo provides a solution on how to include/import Swiper into your Developer Portal without extra effort. “Click to upload” button offers to add a slide to display on Developer Portal. Also, the tenant user can arrange the activation of cards and buttons on the swiper. Title and description content can be added for cards, and buttons' links and labels can be configurated dynamically. The links provide internal connections in Developer Portal. Also, more than one swiper can be created for the home page.

.. figure:: ../../docs/images/ConfigureDeveloperPortalSettings/ConfigureDeveloperPortalSettingsPicture4.png
    :alt: apigo developer portal home swiper

2.4)	How to Customize Home Cards?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The home page is what developers see first after logging in. The home cards can be customized to highlight information specific to the developers. They can be customized by updating Title, selecting Icon, and explaining Description. Three home cards will be defined by default when the tenant is created. The card bodies can be customized to what the priorities of the ApiGo users are. After click on “Save” button, the changes will be displayed on Developer Portal in real-time. “Add New” button is ready to create for more home cards.

.. figure:: ../../docs/images/ConfigureDeveloperPortalSettings/ConfigureDeveloperPortalSettingsPicture5.png
    :alt: apigo developer portal home cards

2.5)	How to Customize API Products?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ApiGo Users are able to manage API products using the ApiGo Management Portal, as described in this section. API products enable you to bundle and distribute your APIs to multiple developer groups simultaneously. API products provide the basis for access control in ApiGo since they provide a management tool for API functions that apps have worked with. The API Product Title, Image, Description, and which link will be followed when clicked on "Docs" button can be updated. Also, new API Products can be created, and outdated ones can be deleted.

.. figure:: ../../docs/images/ConfigureDeveloperPortalSettings/ConfigureDeveloperPortalSettingsPicture6.png
    :alt: apigo developer portal API products

2.6)	How to Customize Home Menus?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The custom menus can be created with Developer Portal Settings. ApiGo provides a menu customization option for tenant users. If you already have an existing menu, you should see it on the Management Portal. Menu’s title and internal link can be added from the Home Menus section. Also, opening in new tab after clicked on and arranging the visibility of the menu can be adjusted in this section. "Add Submenu" button presents another option to add more than one link as submenus under the main menu.

.. figure:: ../../docs/images/ConfigureDeveloperPortalSettings/ConfigureDeveloperPortalSettingsPicture7.png
    :alt: apigo developer portal home menus

2.7)	How to Add Social Login?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Developers can use their Google and GitHub Accounts to sign in to Developer Portal. With social login, the developer will not have to remember individual usernames and passwords to login. Social Login Methods works with similar requirements to enrich Developer Portal's authentication processes. This function can only be used in the environment without mTLS authentication. To take Client ID and Client Secret, use Google Console or GitHub and fill in the related blanks. You can customize button text if you want to change the value created by default. 

.. figure:: ../../docs/images/ConfigureDeveloperPortalSettings/ConfigureDeveloperPortalSettingsPicture8.png
    :alt: apigo developer portal social login

3) You can complete the Developer Portal customization with no React, CSS, or HTML knowledge. ApiGo offers a customization menu to configure all settings of Developer Portal. When updating the Developer Portal theme, you can update not only the look and feel but also functions such as creating menus, products, slides, cards, login methods and so on. ApiGo portals provide a centralized place for developers to discover interactive documentation, code snippets, examples, products, and other functions developers need to successfully use tenant’s APIs.

.. meta::
  :description: Click now to learn how to configure developer portal to the ApiGo Open Banking, to get detailed information and to review our document!