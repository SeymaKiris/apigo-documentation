=============================
`How To XML to JSON?`_
=============================

.. _How To Define an XML to JSON Policy?: https://apigo.com/admin/endpoints

What is XML to JSON Policy?
-------------------------------------

XML to JSON policy converts the response data of institution backends in XML format to JSON format. This ApiGo policy undertakes the conversion from XML to JSON format and provides data return to clients in JSON format.

How It Works?
-------------

1.	Firstly, the endpoint is defined on the Management Portal as in Figure 1. The address of the source system that responds data in XML format is given to the Target URL part of the Endpoint. The Endpoint configurations are saved and published.

`Management Portal`_ -> Endpoints -> Add Endpoint -> Add Policy -> Xml to Json 

.. figure:: ../../docs/images/XmlToJson/XmltoJsonPicture1.PNG
    :alt: apigo endpoints page

    Figure 1

.. figure:: ../../docs/images/XmlToJson/XmltoJsonPicture2.PNG
    :alt: apigo endpoints page

    Figure 2

2.	We recorded the source API which returns the response value shown in Figure 3.  The XML response format of the Endpoint we saved, returns to us as converted to JSON format shown in Figure 4.

.. figure:: ../../docs/images/XmlToJson/XmltoJsonPicture3.PNG
    :alt: apigo header xml to json policy source page

    Figure 3

.. figure:: ../../docs/images/XmlToJson/XmltoJsonPicture4.PNG
    :alt: apigo xml to json postman page

    Figure 4

.. _Management Portal: https://apigo.com/admin

.. _Gateway Logs: https://apigo.com/admin/gateway-logs

.. meta::
  :description: Click now to learn how to transform a header to the ApiGo Open Banking, to get detailed information and to review our document!