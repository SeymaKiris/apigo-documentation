================================
`How To Get an AppToApp Token?`_
================================

.. _How To Get an AppToApp Token?: https://www.postman.com/

1.	Open Postman. In this example the following version is used v7.23.0.

2.	Go to “Preferences” in the main navigation and select “certificates” tab.
    * Postman’s native apps provide a way to view and set SSL certificates on a per domain basis.

3.	To manage your client certificates, click the wrench icon on the right side of the header toolbar, choose "Settings", and select the Certificates tab.

.. figure:: ../../docs/images/HowToGetaTokenWithaCertificate/Figure1.Client_Certificate.png
    :alt: apigo postman settings

4.	To add a new client certificate, click the **Add Certificate** link.

.. figure:: ../../docs/images/HowToGetaTokenWithaCertificate/Figure2.Add_Certificate.png
    :alt: apigo postman certificates

5.	In the **Host** field, enter the domain (without protocol) of the request URL for which you want to use the certificate, for example, ``https://test-ktbankag.identityserver.apigo.market/``.

6.	You can also specify a custom port to associate with this domain in the **Port** field. This is optional. If left empty, the default HTTPS port (443) will be used.

7.	Choose your client certificate file in the **CRT file** field. Currently, Postman only supports the CRT format. Support for other formats (like PFX) will come soon.

.. figure:: ../../docs/images/HowToGetaTokenWithaCertificate/Figure3.Add_Client_Certificate.png
    :alt: apigo postman add certificate

8.	Choose your client certificate key file in the **KEY file** field.
    * If you used a passphrase while generating the client certificate, you’ll need to supply the passphrase in the Passphrase field. Otherwise, leave it blank.
    * Once your certificate is added, it should appear in the client certificates list.

.. figure:: ../../docs/images/HowToGetaTokenWithaCertificate/Figure4.ClientCertificateList.png
    :alt: apigo postman manage certificates

.. note::

    You should not have multiple certificates set for the same domain. If you have multiple ones set, only the last one added will be used.

9.	Add a body: customerId with the value of your API Key (ClientId) 

.. figure:: ../../docs/images/HowToGetaTokenWithaCertificate/Figure5.Request_Body.png
    :alt: apigo postman request body

10.	Run the request with the following result. auth_req_id is available in the response body.

.. figure:: ../../docs/images/HowToGetaTokenWithaCertificate/Figure6.Response_Body.png
    :alt: apigo postman response body

11.	Test the request with the previous parameters.

12.	Open the Postman Console (Main Navigation –> View –> Show Postman console)

.. figure:: ../../docs/images/HowToGetaTokenWithaCertificate/Figure7.Postman_Console.png
    :alt: apigo postman console

13.	Re-run the request and see the results in the postman console to verification
    * If you make a request to a configured domain, the certificate will automatically be sent with the request, provided you make the request over HTTPS.
    * Use https to make sure the certificate is sent.

.. meta::
  :description: Click now to learn how to get an apptoapp token to the ApiGo Open Banking, to get detailed information and to review our document!