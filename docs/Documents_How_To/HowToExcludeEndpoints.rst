==============================
`How To Exclude an Endpoint?`_
==============================

.. _How To Exclude an Endpoint?: https://apigo.com/admin/endpoints

What is Exclude Policy?
-----------------------

Gateway restrictions are a group policy-based feature that identifies applications running on gateway with using defined endpoints and controls the ability of those programs to reach them. Restriction policies are part of the ApiGo's security and management tools to assist ASPSPs in increasing the reliability, integrity, and manageability of their open banking journey. Exclusions may exist because restrictions are thought to be too strict for the gateway users or because they can block the requests which do not constitute an abuse of the APIs.

An exclusion is a policy provision that eliminates coverage for some type of conditions defined as a restriction. Exclusions narrow the scope of coverage provided by the restrictions. In many gateways, the defined endpoints' availability is very broad without restrictions. Tenant admins utilize exclusions for conditions they are unwilling to TPPs reach.

How It Works?
-------------

1) To exclude the restriction for a selected endpoint is an option. Tenant admin can define an Excluded Restrictions Policy for any endpoint the admin need. So, though configured Global Rate Limit, TPP can send a request and take back a response without skipping a beat. This exclusion function can be executed in the endpoint definition screen. To add a new exclusion policy, the specification must be one by one for any endpoint.

`Management Portal`_ -> Endpoints -> Selected Endpoint -> Add Policy -> Exclude Restrictions

.. _Management Portal: https://apigo.com/admin

.. figure:: ../../docs/images/ExcludeEndpoints/ExcludeEndpointsPicture1.png
    :alt: apigo endpoints exclude policy

2) Five restrictions listed in Excluded Restrictions Policy includes Global Rate Limit, Country Restrictions, IP Blacklist, IP Whitelist, Global Rate Limit, Working Hours, Device Threat Protection. To exclude the selected endpoint from the selected restriction, tenant admin clicks on any restriction to define the policy to the endpoint and the changes must be published to respond the related request properly.

.. figure:: ../../docs/images/ExcludeEndpoints/ExcludeEndpointsPicture2.png
    :alt: apigo exclude policy page

3) After tenant admin defined a restriction on Management Portal and click on the “Save” button to make the changes on real, when a request has come to the gateway to reach an endpoint, which is now excluded from the restriction, the following error response will be returned.

.. figure:: ../../docs/images/ExcludeEndpoints/ExcludeEndpointsPicture3.png
    :alt: apigo postman response

4) If Excluded Restrictions Policy has been defined with a selected restriction for an endpoint and the tenant admin has configured the restriction on Management Portal, the response will be appropriately worked successfully only for the selected endpoint.

.. figure:: ../../docs/images/ExcludeEndpoints/ExcludeEndpointsPicture4.png
    :alt: apigo exclude policy response

.. meta::
  :description: Click now to learn how to exclude an endpoint to the ApiGo Open Banking, to get detailed information and to review our document!