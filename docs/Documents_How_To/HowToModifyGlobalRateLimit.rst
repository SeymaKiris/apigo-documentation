===================================
`How To Modify Global Rate Limit?`_
===================================

.. _How To Modify Global Rate Limit?: https://apigo.com/admin/global-rate-limit

What is Global Rate Limit?
--------------------------

ApiGo provides mechanisms that enable you to optimize traffic management to minimize latency for apps while maintaining the health of backend services. Making highly concurrent API calls may result in your app being rate limited. ApiGo’s rate limits can be customized to vary based on the tenant type tenant admin has created. The API rejects requests that exceed the limit. To ensure the quality of endpoints, the tenant admin can arrange restrictions for the APIs with rate limits. The rate limit policy can be excluded depending on the API endpoint.

It is important to ensure that apps do not consume more resources than permitted. The waiting limit can also be configured. This policy can be used to prevent sudden traffic bursts caused by malicious attackers attempting to disrupt a service using a denial-of-service (DOS) attack or by buggy client applications.

How It Works?
-------------

1) To maintain the performance and availability of TPPs, to manage app traffic within the limits of the capacity of defined APIs is critical. The rate limit can be adjusted with a restriction on the Management Portal. To reach the submenu and add a rate limit, use the following path.

`Management Portal`_ -> Restrictions -> Global Rate Limit

.. _Management Portal: https://apigo.com/admin

.. figure:: ../../docs/images/GlobalRateLimit/HowToModifyGlobalRateLimitPicture1.png
    :alt: apigo global rate limit page

2) Tenant admin can configure a delay between retries. A message will be indicated in the rate limit response headers. If the app triggers the rate limit, TPP needs to refrain from making additional requests until the appropriate amount of time has elapsed. Global Rate Limit page provides to enter how many requests will cause to retry, what will be the wait time after a delay is done, what will be the response message to inform the request owner.

.. figure:: ../../docs/images/GlobalRateLimit/HowToModifyGlobalRateLimitPicture2.png
    :alt: apigo edit global rate limit

3) After tenant admin completes the settings of Global Rate Limit, click on the “Save” button to make the changes on real. Also, there is an updating and deleting option whenever you need it. When a request has come to the gateway to reach an endpoint, which includes the global rate limit, the following response will be returned with the HTTP 429 Status code.

.. figure:: ../../docs/images/GlobalRateLimit/HowToModifyGlobalRateLimitPicture3.png
    :alt: apigo global rate limit response

`How to Exclude Global Rate Limit?`_
------------------------------------

.. _How to Exclude Global Rate Limit?: https://apigo.com/admin/endpoints

1) To exclude the restriction for a selected endpoint is another option. Tenant admin can define an Excluded Restrictions Policy for any endpoint the admin need. So, though configured Global Rate Limit, TPP can send a request and take back a response without skipping a beat. This exclusion function can be executed in the endpoint definition screen. To add a new exclusion policy, the specification must be one by one for any endpoint.

Management Portal -> Endpoints -> Selected Endpoint -> Add Policy -> Exclude Restrictions

.. figure:: ../../docs/images/GlobalRateLimit/HowToModifyGlobalRateLimitPicture4.png
    :alt: apigo endpoints page

2) Global Rate Limit is one of five restrictions listed in Excluded Restrictions Policy. To exclude the selected endpoint from the rate limits, select and specialize it to the endpoint. Tenant admin clicks on “Global Rate Limit” to implement the policy for the selected endpoint and the changes must be published to respond the related request properly.

.. figure:: ../../docs/images/GlobalRateLimit/HowToModifyGlobalRateLimitPicture5.png
    :alt: apigo exclude restriction policy

3) If Excluded Restrictions Policy has been defined to exclude rate limit for an endpoint and Global Rate Limit has been configured, the response will be appropriately worked only for the selected endpoint.

.. figure:: ../../docs/images/GlobalRateLimit/HowToModifyGlobalRateLimitPicture6.png
    :alt: apigo global rate limit response

.. meta::
  :description: Click now to learn how to modify global rate limit to the ApiGo Open Banking, to get detailed information and to review our document!