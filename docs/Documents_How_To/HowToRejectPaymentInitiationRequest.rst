======================================
`How To Reject a Payment Initiation?`_
======================================

.. _How To Reject a Payment Initiation?: https://apigo.com/admin/payment-initiations

.. note::

    Business Model: Berlin Group Standard - Requires ApiGo 1.2.1 or later.


What is Rejected Payment Initiation?
------------------------------------

When TPPs are initiating a payment, there are many factors that go into making the payment possibilities and a single glitch in any of these entities can lead to an online transaction failing. The initiation of the payment process maybe wanted to be rejected by the ApiGo user for security concerns or any special reason. The reasons can vary from user to user, and some may have quite aggressive measures when it comes to security, while others may be a bit looser in comparison. There are many reasons why the transaction may be declined. Some of those:

Risk: If there is a security threat by the ApiGo user, this may result in the payment initiation being rejected. An example of where this happens is when a card is marked for domestic use only and is used for an international website. In such cases, the ApiGo user may want to refuse to initiate payment.

Suspicious Transaction: When a suspicious request to initiate a payment is detected by the ApiGo user, the transaction may be requested to be rejected. In such cases, PSU who wants to initiate a payment request may have to do more than one transaction until the desired amount is reached.

Suspicious Account: ApiGo user who detects a transaction coming from a suspicious account may want to decline the initiation of payment. Some accounts may have been identified as suspicious before being blacklisted due to the threat of fraud.

How It Works?
-------------

1) TPP initiates a payment order, with the PSU's explicit consent, from their online payment account held at their ASPSP. As the first step to payment, TPP initiates the payment sends a request to the gateway with a valid access token. For more information about how to get an AppToApp (access_token), click `here`_.

.. _here: https://docs.apigo.com/en/latest/HowToGetanAppToAppToken.html

.. figure:: ../../docs/images/HowToRejectaPaymentInitiationRequest/Picture1_Diagram.png
    :alt: apigo get an apptoapp token

2) After a valid access_token is taken by TPP, a payment initiation request can send to the gateway with the token and payment details in the body. The payment initiation request will be responded with “RCVD” transaction status.

.. figure:: ../../docs/images/HowToRejectaPaymentInitiationRequest/Picture2_RCVD.PNG
    :alt: apigo apptoapp response

3) Payment Initiations & Account Consents can be managed on the Admin Panel. The creation date, the status of the transaction, Payment Service, Payment Product, and PSU’s CustomerId are logged on the Payment Initiations page. Also, to reach more information about the logs, the tenant admin can click on the “Detail” icon.

`Management Portal`_ -> PIS & Account Consent -> Payment Initiation -> Related Payment Initiation Request -> Details

.. _Management Portal: https://apigo.com/admin/

.. figure:: ../../docs/images/HowToRejectaPaymentInitiationRequest/Picture3_ManagementPortal.PNG
    :alt: apigo payment initiation page

4) Tenant admin can reject the relevant payment initiation on the detail page. If the rejection request is approved, the payment initiation will be terminated.

.. figure:: ../../docs/images/HowToRejectaPaymentInitiationRequest/Picture4_Details.PNG
    :alt: apigo payment initiation reject

5) When tenant admin rejects a payment initiation request on Management Portal, the payment transaction status will be updated as “RJCT”. 

.. figure:: ../../docs/images/HowToRejectaPaymentInitiationRequest/Picture5_RJCT.PNG
    :alt: apigo payment initiation rejected response

.. meta::
  :description: Click now to learn how to reject a payment initiation to the ApiGo Open Banking, to get detailed information and to review our document!