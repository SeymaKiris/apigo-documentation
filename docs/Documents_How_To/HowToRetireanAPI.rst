========================
`How To Retire an API?`_
========================

.. _How To Retire an API?: https://apigo.com/admin/endpoints

What is Retiring API Policy?
----------------------------

Banks may choose to discontinue a public API precisely because of the open banking strategy changes. They should go through a careful process of planning and coordination before retiring an API which includes internal data or processes available. 

Replacing API is probably the most common form of API retirement. It includes releasing new versions and consolidating APIs. Since open banking is relatively new for most businesses, we may often see v1, v2, or v3, still in use today. Another typical scenario that may cause an API shutdown is if the API receives limited or unsolicited use by developers and end-users. Sometimes APIs are retired because they are at the end of their useful life.

How It Works?
-------------

1) Deprecation is the final stage for an endpoint and should be done so with care. This retiring APIs process can be managed in Admin Panel. Retiring API policy is added for the selected endpoint and the response message that the API has been retired begins to be shared.

`Management Portal`_ -> Endpoints -> Selected Endpoint -> Add Policy -> Retiring API

.. _Management Portal: https://apigo.com/admin

.. figure:: ../../docs/images/HowToRetireAnAPI/RetireAPI_Picture1.PNG
    :alt: apigo endpoints page

2) The retiring response can be rewrite in the messages text area. After click on “Save” button, the policy will be implemented for the selected endpoint. Finally, the changes must be published to respond the related request with retiring message. 

.. figure:: ../../docs/images/HowToRetireAnAPI/RetireAPI_Picture2.PNG
    :alt: apigo retiring API policy

3) When the TPP sends a request to the endpoint with Retiring API Policy added, the response will be a retiring message. The response messages can give details to migrate users to the new API or inform them about the retired APIs. Tenant admin can inform TPPs about the circumstances that cause the API to not be used. When the given circumstances have been done, the tenant admin may retire the relevant APIs.

**Retiring API Policy has not been set up.**

.. figure:: ../../docs/images/HowToRetireAnAPI/RetireAPI_Picture3.PNG
    :alt: apigo response

**Retiring API Policy has been set up.**

.. figure:: ../../docs/images/HowToRetireAnAPI/RetireAPI_Picture4.PNG
    :alt: apigo response with retiring policy

.. meta::
  :description: Click now to learn how to retire an api to the ApiGo Open Banking, to get detailed information and to review our document!