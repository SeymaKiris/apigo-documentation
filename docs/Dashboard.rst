============
`Dashboard`_
============

.. _Dashboard: https://apigo.com/admin/

After you add new an environment, the dashboard will be accessible. You can monitor daily, weekly, monthly, and yearly statistics. You can monitor gateway traffic that come from country.

.. image:: ../docs/images/Dashboard.png
    :alt: apigo dashboard

Filter Selection
----------------

This function provides to add a limit for your statistics. You can customize your statistics a period.

.. image:: ../docs/images/FilterSelection_Image.png
    :width: 425
    :alt: apigo fiter selection

Language Selection
------------------

You can use ApiGo with two different language option, which is Turkish and English. In the coming days, ApiGo can be used with many other language options.

.. image:: ../docs/images/Language_Image.png
    :width: 425
    :alt: apigo language

Depends on your language selection, the management portal will be shaped.

.. meta::
   :description: Visit our page to learn about ApiGo dashboard, review our document and discover the privileges of the ApiGo Open Banking platform!