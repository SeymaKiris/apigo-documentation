`ApiGo`_: API Management & Open Banking Solution
==================================

ApiGo allows you to share your financial services with fintechs securely while being in accord with open banking regulations quickly and easily.

- **Make Your API Traffic Scalable and Safe**

ApiGo's safe and flexible platform provides to scale your API traffic and protect your service requests and customer data from malicious software.

- **Consent Management for Open Banking**

ApiGo offers you a platform that you can manage consent for open banking. It helps you in the approval process of the customer request sent by fintechs to access financial services.

- **Share Your Services With Developer Portal**

ApiGo offers the opportunity to create unlimited environments that you can use as an API Portal and Sandbox, where fintechs can easily access your APIs.

- **Work in Compliance with Open Banking Standards**

ApiGo offers a platform for financial institutions to comply with legal regulations and open banking standards such as PSD2 and `Berlin Group Standards`_  or `UK Open Banking Group`_.


.. _ApiGo: https://apigo.com/

.. _UK Open Banking Group: https://www.openbanking.org.uk/

.. _Berlin Group Standards: https://www.berlin-group.org/

.. image:: ../docs/images/86-[Converted].png
   :alt: apigo

User Manual
-----------
This documentation has been written to explain the technical features of the ApiGo product and its main purpose.
Learn about **ApiGo** to help you create fantastic customer journey for your business.

This documentation has been prepared for `how can you use ApiGo`_. If you have not get in contact with us.

.. _how can you use ApiGo: https://docs.apigo.com/en/latest/GettingStarted.html

.. toctree::
   :hidden:
   :caption: User Manual

   GettingStarted
   Registration
   Environment
   Dashboard
   Messages
   Configurations
   Endpoints
   Clients
   Documents
   Agreements
   AccountSettings

Restrictions Management
-----------------------

How to define a restriction.

.. toctree::
   :hidden:
   :caption: Restrictions Management

   ../Documents_Restirictions_Management/IPWhiteList
   ../Documents_Restirictions_Management/IPBlacklist
   ../Documents_Restirictions_Management/CountryRestrictions

Quota Management
----------------

Details of Quota Managment consept

.. toctree::
   :hidden:
   :caption: Quota Management

   ../Documents_Quota_Management/01QuotaManagement
   ../Documents_Quota_Management/02ApiBaseQuota
   ../Documents_Quota_Management/03ApplicationBaseQuota   
   ../Documents_Quota_Management/04DomainBaseQuota      

UK Open Banking Specification
-----------------------------

UK Open Banking Specification is designed to assist account providers in meeting their PSD2 and RTS requirements as well as supporting their application for an exemption from the contingency mechanism. This market-enabling Standard is built in an optional modular format to meet consumer and market needs most effectively.

.. toctree::
   :hidden:
   :caption: UK Open Banking Specification

   ../Folder_UK_Open_Bank/AccountAccessPermission   

How To
------

This section aims to be a guide for users `how to use ApiGo`_.

.. _how to use ApiGo: https://docs.apigo.com/en/latest/HowToAddanEnvironment.html

.. toctree::
   :hidden:
   :caption: How To

   ../Documents_How_To/HowToAddanEnvironment
   ../Documents_How_To/HowToGetaClientCredentialToken  
   ../Documents_How_To/HowToGetanAppToAppToken
   ../Documents_How_To/HowToAddMultipleFunctionPolicy
   ../Documents_How_To/HowToAddDataMaskedPolicy
   ../Documents_How_To/HowToCreateaNewTenantUser
   ../Documents_How_To/HowToConfigureDeveloperPortalSettings
   ../Documents_How_To/HowToModifyGlobalRateLimit
   ../Documents_How_To/HowToRetireanAPI
   ../Documents_How_To/HowToRejectPaymentInitiationRequest
   ../Documents_How_To/HowToAddGlobalVariable
   ../Documents_How_To/HowToAddWorkingHoursRestriction
   ../Documents_How_To/HowToTrackGatewayError
   ../Documents_How_To/HowToExportImportEndpoints
   ../Documents_How_To/HowToRejectAccountConsent
   ../Documents_How_To/HowToExcludeEndpoints
   ../Documents_How_To/HowToDefineEndpoint
   ../Documents_How_To/HowToDefineMockPolicy
   ../Documents_How_To/HowToDefineRateLimitPolicy
   ../Documents_How_To/HowToDefineMailPolicy
   ../Documents_How_To/HowToTransformaHeader
   ../Documents_How_To/HowToXmlToJsonPolicy
   ../Documents_How_To/HowToJsonToXmlPolicy


Advanced Features
----------------

`ApiGo`_ offers many advanced features and options. Learn more about these integrations and `how you can get the most out of your APIs`_.

.. toctree::
   :hidden:
   :caption: Advanced Features

   AppToAppAuthentication
   mTLS

.. _how you can get the most out of your APIs: https://docs.apigo.com/en/latest/AppToAppAuthentication.html

Release Note
------------

This is a detailed design for every `releases`_.

.. _releases: https://docs.apigo.com/en/latest/Versions.html

.. toctree::
   :hidden:
   :caption: Release Notes

   Versions
   ../Documents_Relase_Notes/v1.7.3
   ../Documents_Relase_Notes/v1.7.2
   ../Documents_Relase_Notes/v1.7.1
   ../Documents_Relase_Notes/v1.7.0
   ../Documents_Relase_Notes/v1.6.9
   ../Documents_Relase_Notes/v1.6.8
   ../Documents_Relase_Notes/v1.6.7
   ../Documents_Relase_Notes/v1.6.5
   ../Documents_Relase_Notes/v1.6.2
   ../Documents_Relase_Notes/v1.6.0
   ../Documents_Relase_Notes/v1.5.8
   ../Documents_Relase_Notes/v1.5.6
   ../Documents_Relase_Notes/v1.5.4
   ../Documents_Relase_Notes/v1.5
   ../Documents_Relase_Notes/v1.4.6
   ../Documents_Relase_Notes/v1.4.3
   ../Documents_Relase_Notes/v1.4.0
   ../Documents_Relase_Notes/v1.3.0
   ../Documents_Relase_Notes/v1.2.1
   ../Documents_Relase_Notes/v1.1.1
   ../Documents_Relase_Notes/v0.8.6

.. meta::
  :description: Discover ApiGo Open Banking! Visit our document to learn and review details about ApiGo and the End-to-End SaaS Solution.