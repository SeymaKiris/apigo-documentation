========
Versions
========

.. csv-table:: RELEASE NOTES
   :header: CREATION DATE, VERSION
   :widths: 5, 5

   16.12.2021, `v1.7.2`_
   28.09.2021, `v1.7.1`_
   07.09.2021, `v1.7.0`_
   19.08.2021, `v1.6.9`_
   06.08.2021, `v1.6.8`_
   08.07.2021, `v1.6.7`_
   01.06.2021, `v1.6.5`_
   17.05.2021, `v1.6.2`_
   12.04.2021, `v1.6.0`_
   26.02.2021, `v1.5.8`_
   09.02.2021, `v1.5.6`_
   26.01.2021, `v1.5.4`_
   29.12.2020, `v1.5.0`_
   17.11.2020, `v1.4.6`_
   04.11.2020, `v1.4.3`_
   02.10.2020, `v1.4.0`_
   21.09.2020, `v1.3.0`_
   10.08.2020, `v1.2.1`_
   27.07.2020, `v1.1.1`_
   05.02.2020, `v0.8.6`_

.. _v1.7.2: https://docs.apigo.com/en/latest/v1.7.2.html
.. _v1.7.1: https://docs.apigo.com/en/latest/v1.7.1.html
.. _v1.7.0: https://docs.apigo.com/en/latest/v1.7.0.html
.. _v1.6.9: https://docs.apigo.com/en/latest/v1.6.9.html
.. _v1.6.8: https://docs.apigo.com/en/latest/v1.6.8.html
.. _v1.6.7: https://docs.apigo.com/en/latest/v1.6.7.html
.. _v1.6.5: https://docs.apigo.com/en/latest/v1.6.5.html
.. _v1.6.2: https://docs.apigo.com/en/latest/v1.6.2.html
.. _v1.6.0: https://docs.apigo.com/en/latest/v1.6.0.html
.. _v1.5.8: https://docs.apigo.com/en/latest/v1.5.8.html
.. _v1.5.6: https://docs.apigo.com/en/latest/v1.5.6.html
.. _v1.5.4: https://docs.apigo.com/en/latest/v1.5.4.html
.. _v1.5.0: https://docs.apigo.com/en/latest/v1.5.html
.. _v1.4.6: https://docs.apigo.com/en/latest/v1.4.6.html
.. _v1.4.3: https://docs.apigo.com/en/latest/v1.4.3.html
.. _v1.4.0: https://docs.apigo.com/en/latest/v1.4.0.html
.. _v1.3.0: https://docs.apigo.com/en/latest/v1.3.0.html
.. _v1.2.1: https://docs.apigo.com/en/latest/v1.2.1.html
.. _v1.1.1: https://docs.apigo.com/en/latest/v1.1.1.html
.. _v0.8.6: https://docs.apigo.com/en/latest/v0.8.6.html

.. meta::
  :description: Discover the old and new versions of the ApiGo Open Banking platform, the improvements made to the open banking platform.