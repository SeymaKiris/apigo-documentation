==============
`Domain Base Quota`_
==============

This type of quota management is also known as global quota management. It is the most common feature in similar API management 
applications. A general quota definition is made for your environments such as dev, pre-prod or prod, and a value is calculated 
by summing all requests to the system.

**Advantages**

-   You can define quota very easily.
-   Since it is not a complex structure, detailed calculations and adjustments are not required.

**Disadvantages**

-   Does not meet B2B needs.
-   Does not meet API specific quota needs.
-   Requests to all API resources from all applications will be interrupted when the global quota is reached.

**How to configure Domain Based Quota?**

In order to manage Domain Based quotas in ApiGo, you must first log in to the administration panel with your ApiGo administrative account and follow the steps below.

-   Click Configurations -> Global Limit Configurations from the left menu. From the screen that opens, navigate to the Usage Quota section.

.. figure:: ../../docs/images/Quota/08_Quota_Policy.png
    :alt: Global Quota configuration

The parameters specified in the Usage Quota definition and their explanations are as follows.

-   **Enable Usage Quata**: It is used to activate or deactivate the quota application for this application.
-   **Max request per period**: defines how many requests can be made for this API in a given period below.
-   **Quota reset every**: It is used to determine what the quota period is. 4 different periods can be defined as Day, Week, Month and Year.
-   **Error Message**: What should be the response message that the API will produce in case of exceeding the quota.

After this configuration is defined, requests made from all applications to all APIs defined on the Gateway are included in the quota calculation.