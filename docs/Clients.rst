===============
`Applications`_
===============

.. _Applications: https://apigo.com/admin/clients

The developer portal provides to create applications for the developers with your APIs. They empower their applications with the APIs which take place in your portal.

Create an Application
---------------------

With the application function, the developers can create an application. Click on “Add Application” button.

.. image:: ../docs/images/App1.png
    :alt: apigo application page


In Add Application page, you can add an icon, client name, optionally a description, certificate, public key, Redirection Uri. Also, you can select the scope, including confirmation of funds, payment initiation, account, and transaction. After the acceptance of Terms&Conditions agreement, click on the “Add Client” button. Finally, the application will be available on the application page. You can copy ID, secret, and scopes by clicking on their icons.

.. image:: ../docs/images/AddApplication.png
    :alt: apigo add application

View the Applications
---------------------

The applications which are created in the developer portal can be monitored on the Applications' page. You can manage the applications on this page. 

.. image:: ../docs/images/ViewClients1.png
    :alt: apigo view application

.. meta::
   :description: Visit our page to learn about ApiGo applications, review our document and discover the privileges of the ApiGo Open Banking platform!